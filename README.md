# Predict the salary of Data Scientist based on his working experience in years

Steps are involved in this well-defined ML project:
1. Understand and define the problem
2. Analyse and prepare the data
3. Apply the algorithms
4. Reduce the errors
5. Predict the result